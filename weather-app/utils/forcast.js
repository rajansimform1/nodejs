import request from "request";

// Rajkot LatLon 22.3039° N, 70.8022° E

// const url =
//   "https://api.openweathermap.org/data/2.5/weather?lat=22.3039&lon=70.8022&appid=57f2bf70cbc522f235f8030b4730bc81";

// const url2 =
//   "http://api.weatherstack.com/current?access_key=7f16feb6f90fb105b46f7a9e8b395b33&query=rajkot";

export const forcast = (lat, lon, cb) => {
  const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=57f2bf70cbc522f235f8030b4730bc81`;

  //json:true will parse response to json format
  request({ url, json: true }, (err, { body }) => {
    // const data = JSON.parse(response.body);
    if (err) {
      cb("Unable to use API now...", undefined);
    } else if (body.message) {
      cb("Cant Able to find Location....", undefined);
    } else {
      cb(undefined, body);
    }
  });
};
