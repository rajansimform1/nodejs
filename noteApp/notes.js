import fs from "fs";
// fs.writeFileSync("notes.txt","hello there");

export const addNote = (title, body) => {
  const notes = loadNotes();
  const duplication = notes.find((obj) => obj.title === title);
  //find method stops when first occurance happens
  if (!duplication) {
    notes.push({
      title: title,
      body: body,
    });
    saveNote(notes);
    console.log(`File "${title}" added succesfully`);
  } else {
    console.log(`File Name: "${title}" already exist !!!`);
  }
};

export const removeNote = (title) => {
  const notes = loadNotes();
  const found = notes.findIndex((obj) => obj.title === title);
  if (found !== -1) {
    notes.splice(found, 1);
    saveNote(notes);
    console.log(`File "${title}" was removed succesfully`);
  } else {
    console.log(`No such file exist named: "${title}"`);
  }
};

export const readNote = (title) => {
  const notes = loadNotes();
  const found = notes.findIndex((obj) => obj.title === title);
  if (found !== -1) {
    console.log(
      "",
      `File name: \t ${title} \n`,
      "Body: \t\t",
      `${notes[found].body}`
    );
  } else {
    console.log(`No such file exist named: "${title}"`);
  }
};

export const listNotes = () => {
  const notes = loadNotes();
  console.log("Your Notes:");
  notes.forEach((note) => {
    console.log(note.title);
  });
};

const loadNotes = () => {
  try {
    const data = fs.readFileSync("notes.json").toString();
    return JSON.parse(data);
  } catch (e) {
    return [];
  }
};

const saveNote = (notes) => {
  const dataJSON = JSON.stringify(notes);
  fs.writeFileSync("notes.json", dataJSON);
};
