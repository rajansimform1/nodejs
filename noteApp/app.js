import chalk from "chalk";
import yargs from "yargs";
import { addNote, removeNote, listNotes, readNote } from "./notes.js";

// console.log(chalk.red.inverse.bold("hello there"));

//add command
yargs.command({
  command: "add",
  describe: "Add a note",
  builder: {
    title: {
      describe: "title of the note",
      demandOption: true,
      type: "string",
    },
    body: {
      describe: "body of the note",
      demandOption: true,
      type: "string",
    },
  },
  handler: (argv) => {
    console.log(`title:${argv.title} , body: "${argv.body}"`);
    addNote(argv.title, argv.body);
  },
});

//remove command
yargs.command({
  command: "remove",
  describe: "Remove a note",
  builder: {
    title: {
      describe: "title of the note",
      demandOption: true,
      type: "string",
    },
  },
  handler: (argv) => {
    console.log(`title:${argv.title}`);
    removeNote(argv.title);
  },
});

//read command
yargs.command({
  command: "read",
  describe: "Read a note",
  builder: {
    title: {
      describe: "title of the note",
      demandOption: true,
      type: "string",
    },
  },
  handler: (argv) => {
    readNote(argv.title);
  },
});

//list command
yargs.command({
  command: "list",
  describe: "Display a list of notes",
  handler: listNotes,
});

yargs.parse();
// console.log(yargs.argv);
